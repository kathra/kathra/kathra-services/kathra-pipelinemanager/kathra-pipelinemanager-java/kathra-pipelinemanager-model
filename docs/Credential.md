
# Credential

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | Pipeline path |  [optional]
**credentialId** | **String** | Credential ID |  [optional]
**username** | **String** | Username associated to the credential |  [optional]
**description** | **String** | Credential description |  [optional]
**password** | **String** | Password |  [optional]
**privateKey** | **String** | Private key content |  [optional]



