/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.pipelinemanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* Credential
*/
@ApiModel(description = "Credential")
public class Credential {

    protected String path = null;
    protected String credentialId = null;
    protected String username = null;
    protected String description = null;
    protected String password = null;
    protected String privateKey = null;


    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myCredential.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Credential The modified Credential object
    **/
    public Credential path(String path) {
        this.path = path;
        return this;
    }

    /**
    * Pipeline path
    * @return String path
    **/
    @ApiModelProperty(value = "Pipeline path")
    public String getPath() {
        return path;
    }

    /**
    * Pipeline path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set credentialId
    * Useful setter to use in builder style (eg. myCredential.credentialId(String).anotherQuickSetter(..))
    * @param String credentialId
    * @return Credential The modified Credential object
    **/
    public Credential credentialId(String credentialId) {
        this.credentialId = credentialId;
        return this;
    }

    /**
    * Credential ID
    * @return String credentialId
    **/
    @ApiModelProperty(value = "Credential ID")
    public String getCredentialId() {
        return credentialId;
    }

    /**
    * Credential ID
    **/
    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    /**
    * Quick Set username
    * Useful setter to use in builder style (eg. myCredential.username(String).anotherQuickSetter(..))
    * @param String username
    * @return Credential The modified Credential object
    **/
    public Credential username(String username) {
        this.username = username;
        return this;
    }

    /**
    * Username associated to the credential
    * @return String username
    **/
    @ApiModelProperty(value = "Username associated to the credential")
    public String getUsername() {
        return username;
    }

    /**
    * Username associated to the credential
    **/
    public void setUsername(String username) {
        this.username = username;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myCredential.description(String).anotherQuickSetter(..))
    * @param String description
    * @return Credential The modified Credential object
    **/
    public Credential description(String description) {
        this.description = description;
        return this;
    }

    /**
    * Credential description
    * @return String description
    **/
    @ApiModelProperty(value = "Credential description")
    public String getDescription() {
        return description;
    }

    /**
    * Credential description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set password
    * Useful setter to use in builder style (eg. myCredential.password(String).anotherQuickSetter(..))
    * @param String password
    * @return Credential The modified Credential object
    **/
    public Credential password(String password) {
        this.password = password;
        return this;
    }

    /**
    * Password
    * @return String password
    **/
    @ApiModelProperty(value = "Password")
    public String getPassword() {
        return password;
    }

    /**
    * Password
    **/
    public void setPassword(String password) {
        this.password = password;
    }

    /**
    * Quick Set privateKey
    * Useful setter to use in builder style (eg. myCredential.privateKey(String).anotherQuickSetter(..))
    * @param String privateKey
    * @return Credential The modified Credential object
    **/
    public Credential privateKey(String privateKey) {
        this.privateKey = privateKey;
        return this;
    }

    /**
    * Private key content
    * @return String privateKey
    **/
    @ApiModelProperty(value = "Private key content")
    public String getPrivateKey() {
        return privateKey;
    }

    /**
    * Private key content
    **/
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credential credential = (Credential) o;
        return Objects.equals(this.path, credential.path) &&
        Objects.equals(this.credentialId, credential.credentialId) &&
        Objects.equals(this.username, credential.username) &&
        Objects.equals(this.description, credential.description) &&
        Objects.equals(this.password, credential.password) &&
        Objects.equals(this.privateKey, credential.privateKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, credentialId, username, description, password, privateKey);
    }
}

